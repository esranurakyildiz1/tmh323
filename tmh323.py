#!/usr/bin/env python
# coding: utf-8

# # Hafta 1

# * platform ve kaynak tanıtımı
# * Temel kavramlar

# # Hafta 2 Veri seçimi, verilerin hazırlanması

# Jupyter tanıtımı

# ## Python a hızlı bir başlangıç

# ### Print fonksiyonu

# Komut satırına veya dosyaya yazmayı sağlar. En çok kullanılan parametresi "sep" parametresidir. Defult olarak" " belirlenmiştir. Diğer bir sık kullanılan parametre ise end parametresidir.

# In[1]:


print("selamın alikim")


# In[2]:


print("hakan hocayla","eğlenceli saatler",sep="=>")


# TEk tırnak, çift tırnak ve üçlü tırnak olarak kullanılabilir. Bunun yanında comment için# işareti kullanılabilir.

# ### Type ve len fonksiyonu

# type: Kullanılan verinin veya değişken tipini verir.
# len:kullanılan dizinin uzunluğunu verir

# In[3]:


type("deneme")


# In[4]:


type(5.1)


# In[5]:


len("hellöö")


# ### Değişken ve değişken belirleme kuralları

# Değişken belirleme kuralları:
# * Sayı ile başlayamaz.
# * Aritmetik işaretler ile başlayamaz.(+-*/)
# * Boşluk kullanılmamalıdır.
# * Büyük küçük harf ayırımına dikkat edilmelidir.
# * Özel kelimeler kullanılmamalıdır.(if,for,true)
# * Alfabe harfi veya _ile başlayabilir.
# * Olabildiğince tanımlayıcı ve okunabilir seçilmelidir.
# 
# Değişken olarak kullanılamayacak kelimeler aşağıdaki kod ile belirlenebilir.

# In[6]:


import keyword
yasakliKelimeler=keyword.kwlist
print(yasakliKelimeler)


# In[8]:


a=b=c=0
x="yazılım"
y="donanım"
x,y=y,x
print(x)
print(3**0.5)


# ### Karşılaştırma operatörleri ve koşullar

# |Operatör|Anlamı|
# |---------|------|
# |<|küçüktür|
# |>|büyüktür|
# |<=|küçük eşittir|
# |>=|büyük eşittir|
# |==|eşittir|
# |!=|eşit değildir|

# * if
# * elif
# * else

# In[9]:


a=1
if a<0:
    print("negatif bro")
elif a>0:
    print("pozi agam")
else:
    prit("zıfır")


# ### Matemetiksel operatörler
# "+","-","*","/","%","//","**"

# In[11]:


a=8
b=5
print(a+b)
print(a-b)
print(a*b)
print(a/b)
print(a%b)
print(a//b)
print(a**b)


# In[12]:


vize=70
final=62
ortalama=vize*0.4+final*0.6
if final<50:
    print("finalin nanay")
elif ortalama<50:
    print("ortalaman yetmiyor agam")
else:
    print("büte girmiyon hadi yine iyisn.ort:",ortalama)


# ### Döngüler
# for ve while olmak üzere iki ana döngü kullanılır.
# 

# #### while
# koşul doğru olduğu sürece çalışır

# In[13]:


a=0
while a<10:
    a+=1
    print(a, end=" ")


# In[14]:


a=0
while a<100:
    a+=1
    if a%4==0:
        print(a,end=" ")


# #### for
# in dizi veya in range olarak kullanılır.

# In[15]:


dersAdi="Makina şeysi"
for harf in dersAdi:
    print(harf,end=" ")


# range 3 parametre alabilir
# * 1 parametre:0 dan başlar ve bitişi gösterir.
# * 2 parametre:başlangıç ve bitişi gösterir.
# * 3 parametre: başlangıç bitiş ve aralığı gösterir.

# In[16]:


for i in range(len(dersAdi)):
    print(dersAdi[i],end=" ")


# In[17]:


for i in range(5,20):
    print(i,end=" ")


# In[19]:


sayac=0
for harf in dersAdi:
    sayac+=1
print(sayac)


# ### List
# Diğer programlama dillerindeki dizilerin benzeridir. Köşeli parantez ile tanımlanır. İndis numarası 0 dan başlar ve indis numarası ile istenen elmeza ulaşılır.

# In[21]:


liste=[]
liste2=[1,2,["Mutlu","Esra"],"<3",3.14]# farklı veri tipleri kullanılabilir.
liste2


# In[26]:


liste2[3:2]
liste2[-1]
liste2[0:2]
liste2[::-1]
len(liste2)


# append ve extend ile listeye eleman eklenir

# In[31]:


list1=[1,2,3]
list2=[4,5,6]
list1.append(list2)
print(list1)

list1=[1,2,3]
list2=[4,5,6]
list1.extend(list2)
print(list1)


# ### Tuple
# listelere çok benzeyen kapsayıcı veri tipleridir. Parantez ile tanımlanır. listeler->mutable,tuple->immutable

# In[33]:


demet=()
demet2=(1,2,"mutlu<3","esra")
demet2[-1]#öğelere erişim listelerdeki gibi yapılmaktadır.


# ### Dict 
# Bllok parantez{} ile tanmlanır. İndis(key) ve valuable birlikte tanımlamaya yarayan kapsayıcı veri tipleridir.

# In[34]:


sozluk={"kitap":"book","defter":"notebook"}
print(sozluk["kitap"])


# In[35]:


for i in sozluk: #tüm keyleri yazdırma
    print(i)
for i in sozluk: #tğm value ları yazdırma
    print(sozluk[i])


# In[36]:


sozluk["kalem"]="pen"
print(sozluk)


# In[37]:


sozluk["kalem"]="pencil"
print(sozluk)


# > mutable: sözlükler,listeler -- immutable: sayılar, stringler, tuple

# > sözlük oluştururken value için istenen veri tipi kllanılabilir. ancak keyler için immutable olması gerekir

# ### Fonksiyonlar
# * Her fonksiyonun bir adı vardır ve bununla çağrılırlar.
# * Yapı olarak hepsinin yanında parametrelerin yazıldığı parantez bulunur.
# * Farklı sayıda parametre tanımlanabilri,zorunlu değildir.
# * kullanılabilmesi için mutlaka tanımlanmış olması gerekir.
# * def ifadesi ile tanımlanırlar.
# * fonksiyon adları için değişken kuralları geçerlidir.
# * return ifadesi ile sonuç döndürülebilir.

# In[38]:


def selamYazdir():
    print("Selammss")
selamYazdir()


# In[39]:


#verilen metnin uzunluğunu bulduran fonks.
def uzunluk(metin):
    return len(metin)
text="deniyoruz bişiler"
print(uzunluk(text))


# In[40]:


import math
def hesapla(r):
    cevre=2*math.pi*r
    alan=math.pi*r**2
    hacim=4/3*math.pi*pow(r,3)
    return cevre,alan,hacim
r=5
gelenCevre,gelenAlan,gelenHacim=hesapla(r)
print(gelenCevre,gelenAlan,gelenHacim)
type(hesapla(r))


# In[42]:


#verilen iki nokta arası uzaklık
a=[57,46]
b=[12,34]
def uzaklikHesapla(p1,p2):
    mesafe=((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)**0.5
    return mesafe
gelen=uzaklikHesapla(a,b)
print(gelen)


# In[ ]:




